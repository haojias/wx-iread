const book_search = require('../../services/book_search.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        inputText: '',
        searches: [],
        showSearches: false, //是否显示热门搜索
        down: false, // 显示加载更多
        list:[],
        pageNum: 1,
        pageSize: 10,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.popularSearches()
    },

    /**
     * 加载热门数据 一条
     */
    popularSearches: function () {
        let that = this
        book_search.popularSearches(null, (res) => {
            if (res.code == 0) {
                if (res.data.length > 0) {
                    that.setData({
                        searches: res.data,
                        showSearches: true
                    })
                } else {
                    that.setData({
                        showSearches: false
                    })
                }
            }
        })
    },

    /**
     * 主动搜索
     */
    search: function () {
        wx.showLoading({
            title: '加载中',
        })
        let that = this
        let data = {
            searchTxt: this.data.inputText,
            pageNum: this.data.pageNum,
            pageSize: this.data.pageSize,
        }
        book_search.searchList(data, (res) => {
            if (res.code == 0) {

                let d = that.data.list
                let newRes = d.concat(res.data.data)

                that.setData({
                    list: newRes
                })
                if (res.data.isHasNextPage) {
                    that.setData({
                        down: true,
                        pageNum: that.data.pageNum + 1
                    })
                } else {
                    that.setData({
                        down: false
                    })
                }
            }
            wx.hideLoading()
        })
    },

    loadMore: function () {
        this.search()
    },

    /**
     * 获取输入的值
     */
    getText: function (e) {
        this.setData({
            inputText: e.detail.value
        })
    },

    /**
     * 获取到焦点后 清除热门内容
     */
    clearText: function (e) {
        this.setData({
            input_value: '',
            inputText: ''
        })
    },

    /**
     * 跳转到书籍详情
     * @param e
     */
    toDetails: function (e) {
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '../details/details?id=' + id
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})