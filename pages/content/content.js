// pages/content/content.js
const catalog = require('../../services/catalog.js');
const utils = require('../../utils/util.js');

Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: "",
        bookInfoId: '',
        data: '',
        current: 1, // 当前打开的目录索引
        close: false, //  目录
        openText: false, // 字体
        sets: false, // 设置
        fontSize: 34,
        show: false,
        previous: false,
        previousChapter: '',
        nextChapter: '',
        next: false,
        catalog: [],
        scrollTop :0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let size = wx.getStorageSync('fontSize')
        if(size == ''|| size ==undefined || size == null){
          size = 34
        }
        console.log('ssssss --- '+size)
        this.setData({
            id: options.id,
            current: options.current,
            bookInfoId: options.bookInfoId,
            fontSize: size
        })
        this.getContent(options.id)
    },

    /**
     * 页面内目录刷新内容
     * @param e
     */
    toContent:function(e){
        let id = e.currentTarget.dataset.id
        this.getContent(id)
        this.setData({
            openText: false,
            sets: false
        })

        if (this.data.close) {
            this.close()
        } else {
            this.setData({
                close: true, // true代表打开
            })
        }
    },

    /**
     * 获取内容
     * @param id
     */
    getContent: function (id) {
        wx.showLoading({
            title: '内容加载中',
        })
        let that = this
        let data = {
            bookCatalogId: id
        }
        catalog.getContent(data, (res) => {
            if (res.code == 0) {
                that.setData({
                    data: res.data.content,
                    previous: res.data.previous,
                    next: res.data.next,
                    previousChapter: res.data.previousChapter,
                    nextChapter: res.data.nextChapter,
                    show: true,
                    scrollTop:0
                })
                wx.setNavigationBarTitle({
                    title: res.data.catalogName
                })
            } else {
                that.setData({
                    data: '抱歉！！章节内容正在等待上传。',
                    show: true
                })
            }
            wx.hideLoading()
          
        })
    },

/**
 *监控滑动
 */
  bindscroll:function(e){
    let scrollTop = e.detail.scrollTop
    if(scrollTop==1){
        this.setData({
            scrollTop: scrollTop
        })
    }
    if(scrollTop==50){
        this.setData({
            scrollTop: scrollTop
        })
    }
    if(scrollTop==300){
        this.setData({
            scrollTop: scrollTop
        })
    }
    if(scrollTop==600){
        this.setData({
            scrollTop: scrollTop
        })
    }
  },

    //加载上\下一章内容
    toChapter: function (e) {
        let type = e.currentTarget.dataset.type
        if (type == 0) {
            if (this.data.previous){
                this.getContent(this.data.previousChapter)
            } else {
                utils.showSuccessToast("没啦")
            }

        } else {
            if (this.data.next){
                this.getContent(this.data.nextChapter)
            } else {
                utils.showSuccessToast("没啦")
            }
        }
    },


    /**
     * 打开设置
     */
    openSets: function () {
        if (this.data.sets) {
            this.close()
        } else {
            this.setData({
                sets: true, // false代表关闭
            })
        }
    },

    /**
     * 打开目录 首先关闭所有的 其他弹出框
     */
    openCatalog: function () {

        // 没有值的时候 去加载值
        if (this.data.catalog.length < 1) {
            wx.showLoading({
                title: '目录加载中',
            })

            let data = {
                id: this.data.bookInfoId
            }

            catalog.findList(data, (res) => {
                if (res.code == 0) {
                    this.setData({
                        close: true, // true代表打开
                        catalog: res.data
                    })
                    wx.hideLoading()
                }
            })
        }
        this.setData({
            openText: false,
            sets: false
        })

        if (this.data.close) {
            this.close()
        } else {
            this.setData({
                close: true, // true代表打开
            })
        }
    },

    /**
     * 打开字体设置
     */
    openText: function () {
        this.setData({
            close: false,
            sets: false
        })
        if (this.data.openText) {
            this.close();
        } else {
            this.setData({
                openText: true, // false代表关闭
            })
        }
    },

    close: function () {
        this.setData({
            openText: false,
            sets: false,
            close: false, // false代表关闭
        })
    },


    /**
     * 设置字体大小
     */
    text_change: function (e) {
        if (e.currentTarget.dataset.current == 1) {
            let res = this.data.fontSize - 2
            if (res > 30) {
                this.setData({
                    fontSize: res
                })
                wx.setStorageSync('fontSize', res)
            }
        } else {
            let res = this.data.fontSize + 2
            if (res < 42) {
                this.setData({
                    fontSize: res
                })
                wx.setStorageSync('fontSize', res)
            }
        }
    },


    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },


    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})