const book = require('../../services/book.js');
const catalog = require('../../services/catalog.js');
const popularity = require('../../services/popularity.js');
const bookshelf = require('../../services/bookshelf.js');
const app = getApp()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        close: false, // false代表关闭
        info: {},
        catalog: [],
        bookshelf: false,
        bookshelf_text: '加入书架',
        bookshelf_text_ed: '已加入',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (option) {
        this.getInfo(option.id)
    },

    /**
     * 加入书架
     */
    addBookshelf: function () {
        let that = this
        let data = {
            bookInfoId: this.data.info.id
        }
        bookshelf.bookshelf(data, (res) => {
            console.log(res)
            if (res.code == 0) {
                that.setData({
                    bookshelf: true
                })
                wx.showToast({
                    title: res.msg,
                })
            }else {
                wx.showToast({
                    title: res.msg,
                    icon:'error'
                })
            }

        })
    },

    /**
     * 阅读按钮事件
     *  首先检索 阅读记录
     *  没有登录 就
     */
    toRead: function () {
        let id = this.data.info.id
        let data = {
            bookInfoId: id
        }
        catalog.readBookByCatalog(data, (res) => {
            if (res.code == 0) {
                let id = res.data.id
                let current = res.data.catalogIndex
                let bookInfoId = this.data.info.id
                wx.navigateTo({
                    url: '../content/content?id=' + id + "&current=" + current + "&bookInfoId=" + bookInfoId
                })
            }else {
                wx.showToast({
                    title: '未检索到目录',
                })
            }
        })
    },

    /**
     * 加载详情
     */
    getInfo: function (id) {
        wx.showLoading({
            title: '加载中',
            mask: true
        })
        let that = this
        let data = {
            id: id
        }
        book.getInfo(data, (res) => {
            if (res.code == 0) {
                that.setData({
                    info: res.data,
                    bookshelf: res.data.bookshelf
                })
                wx.setNavigationBarTitle({
                    title: res.data.name
                })
            }
            wx.hideLoading()
        })
    },

    /**
     * 打开目录页面
     */
    openCatalog: function () {

        if (this.data.catalog.length > 0) {
            this.setData({
                close: true, // true代表打开
            })
            return false;
        }

        wx.showLoading({
            title: '目录加载中',
        })

        let data = {
            id: this.data.info.id
        }

        catalog.findList(data, (res) => {
            if (res.code == 0) {
                this.setData({
                    close: true, // true代表打开
                    catalog: res.data
                })
                wx.hideLoading()
            } else {
                wx.hideLoading()
                wx.showToast({
                    title: '未检索到目录',
                })
            }
        })
    },

    /**
     * 关闭目录页面
     */
    close: function () {
        this.setData({
            close: false, // false代表关闭
        })
    },

    /**
     * 跳转到内容页面
     */
    toContent: function (e) {
        this.setData({
            close: false
        })
        let id = e.currentTarget.dataset.id
        let current = e.currentTarget.dataset.current
        let bookInfoId = this.data.info.id

        wx.navigateTo({
            url: '../content/content?id=' + id + "&current=" + current + "&bookInfoId=" + bookInfoId
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        let i = 0;
        let that = this
        let timer = setInterval(function () {
            if (i == 10) {
                clearInterval(timer)
                let data = {
                    bookInfoId: that.data.info.id,
                    userId: wx.getStorageSync("uid")
                }
                popularity.addPopularity(data, (res) => {
                })
            }
            i = i + 1
        }, 1000);
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})