const classification = require('../../services/classification.js');
const book = require('../../services/book.js');
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: '',
        selected: 0,
        categorys: [],
        books: [],
        pageNum: 1,
        pageSize: 10,
        down: true,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.findCategory();
    },

    findCategory: function () {
        let that = this
        classification.findList(null, (res) => {
            if (res.code == 0) {
                that.setData({
                    categorys: res.data,
                    id: res.data[0].id
                })
                wx.setNavigationBarTitle({
                    title: res.data[0].name
                })
                that.findBooks(res.data[0].id)
            }
        })
    },

    /**
     * 切换选中
     * @param e
     */
    toChange: function (e) {
        let index = e.currentTarget.dataset.index
        let id = e.currentTarget.dataset.id
        this.setData({
            id: id,
            selected: index,
            pageNum: 1,
            books: []
        })
        wx.setNavigationBarTitle({
            title: this.data.categorys[index].name
        })
        this.findBooks(id)
    },

    /**
     * 分页查询书籍信息
     * @param id
     */
    findBooks: function (id) {
        wx.showLoading({
            title: '加载中',
            mask: true
        })
        let that = this
        let data = {
            categoryId: id,
            pageNum: this.data.pageNum,
            pageSize: this.data.pageSize,
        }

        book.findList(data, (res) => {
            if (res.code == 0) {
                let book = that.data.books
                let newRes = book.concat(res.data.list)
                that.setData({
                    books: newRes,
                    // 执行成功后加1页
                    pageNum: that.data.pageNum + 1
                })
              // 可下拉
              this.setData({
                down: res.data.isHasNextPage
              })
            }

            wx.hideLoading()
        })
    },

    /**
     * 上拉触底时触发
     */
    bindscrolltolower: function () {
        if (this.data.down) {
            this.setData({
                down: false
            })
            this.findBooks(this.data.id)
        }
    },

    /**
     * 跳转到详情页
     */
    toDetails: function (e) {
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '../details/details?id=' + id
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})