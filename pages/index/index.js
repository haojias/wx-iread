// pages/index/index.js
const record = require('../../services/record.js');
const popularity = require('../../services/popularity.js');
const book = require('../../services/book.js');
const carousel = require('../../services/carousel.js');
//获取应用实例
const app = getApp()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        readMax: {},
        readMaxList: [],
        popularityList: [],
        endBookList: [],
        carousels: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.carousel()
        this.findReadMax()
        this.frontPopularityInfo()
        this.endBookList()
    },

    /**
     * 轮播图接口
     */
    carousel: function () {
        let that = this
        carousel.carousel(null, (res) => {
            if (res.code == 0) {
                that.setData({
                    carousels: res.data
                })
            }
        })
    },

    /**
     * 10条随机书籍列表
     */
    endBookList: function () {
        let that = this
        book.endBookList(null, (res) => {
            if (res.code == 0) {
                that.setData({
                    endBookList: res.data
                })
            }
        })
    },

    /**
     * 人气值最大的书籍4条
     */
    frontPopularityInfo: function () {
        let that = this
        popularity.frontPopularityInfo(null, (res) => {
            if (res.code == 0) {
                that.setData({
                    popularityList: res.data
                })
            }
        })
    },

    /**
     * 阅读量最大的书籍
     */
    findReadMax: function () {
        let that = this
        record.userRecordMax(null, (res) => {
            if (res.code == 0) {
                that.setData({
                    readMax: res.data.maxRead,
                    readMaxList: res.data.maxList
                })
            }
        })
    },

    /**
     * 跳转到详情页
     */
    toDetails: function (e) {
        console.log('eeeeee:' + e.currentTarget.dataset)
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '../details/details?id=' + id
        })
    },


    /**
     * 跳转到人气列表页
     */
    toFindPopularityList: function () {
        wx.navigateTo({
            url: '../list/list?type=' + type
        })
    },

    /**
     * 跳转到推荐列表 、热门列表页面
     */
    toList: function (e) {
        let type = e.currentTarget.dataset.type
        wx.navigateTo({
            url: '../list/list?type=' + type
        })
    },

    /**
     * 跳转到搜索页面
     */
    toSearch: function () {
        wx.navigateTo({
            url: '../search/search'
        })
    },

    /**
     * 跳转到分类页面
     */
    toClass: function () {
        wx.navigateTo({
            url: '../class/class'
        })
    },


    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.findReadMax()
        this.frontPopularityInfo()
        this.endBookList()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
