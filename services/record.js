const util = require('../utils/util.js');
const api = require('../config/api.js');

function createReadRecord(data, callback) {
    util.request(api.createReadRecord, data, 'POST', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function findUserRecordList(data, callback) {
    util.request(api.userRecordList, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function userRecordMax(data, callback) {
    util.request(api.userRecordMax, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

module.exports = {
    createReadRecord,
    findUserRecordList,
    userRecordMax,
};