const util = require('../utils/util.js');
const api = require('../config/api.js');

function findList(data, callback) {
    util.request(api.book, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function getInfo(data, callback) {
    util.request(api.bookInfo, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

function endBookList(data, callback) {
    util.request(api.endBookList, data, 'GET', (res) => {
        typeof callback == "function" && callback(res)
    })
}

module.exports = {
    findList,
    getInfo,
    endBookList,
};